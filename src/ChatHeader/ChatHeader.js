import React from 'react';
import './ChatHeader.css';

import { connect } from 'react-redux';

class ChatHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      lastMessageTime: '12:34'
    }
  }

  render() {
    const data = this.state;
    return (
      <div className="ChatHeader">
        <div className="ChatHeaderContent">
          <div className="ChatHeaderLeft">
            <div className="ChatName">
              <strong>TopSecret</strong>
            </div>
            <div className="ParticipantsCount">
              <strong>420 participants</strong>
            </div>
            <div className="MessageCount">
              <strong>{this.props.messages.length} messages</strong>
            </div>
          </div>
          <div className="ChatHeaderRight">
            <i>Last message at: {this.props.messages[this.props.messages.length - 1].createdAt}</i>
          </div>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    messages: state.messageList
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ChatHeader);
