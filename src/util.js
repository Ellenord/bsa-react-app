export function beautyTime(time) {
  if (time == '') {
    return time;
  }
  return time.slice(11, 13) + ':' + time.slice(14, 16) + ':' + time.slice(17, 19) + ' '
       + time.slice(0, 4) + '.' + (Number(time.slice(5, 7)) - 1) + '.' + time.slice(8, 10);
};

export function to2char(num) {
  if (num == 0) {
    return '00';
  } else if (num < 10) {
    return '0' + num;
  } else {
    return String(num);
  }
};

export function getBeautyTime() {
  let time = new Date();
  return to2char(time.getHours()) + ':' + to2char(time.getMinutes()) + ':' + to2char(time.getSeconds()) + ' '
       + time.getFullYear() + '.' + to2char(time.getMonth()) + '.' + to2char(time.getDate());
}
