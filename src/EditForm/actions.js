import { SHOW_FORM, SAVE_CHANGES, CANCEL_CHANGES } from './actionTypes';

export const showForm = (message) => ({
  type: SHOW_FORM,
  payload: {
    message
  }
});

export const saveChanges = (text) => ({
  type: SAVE_CHANGES,
  payload: {
    text
  }
});

export const cancelChanges = () => ({
  type: CANCEL_CHANGES
});
