import React from 'react';
import './EditForm.css';

import { connect } from 'react-redux';
import { updateMessage } from '../MessageList/actions';
import { saveChanges, cancelChanges } from './actions';

class EditForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputFieldValue: ''
    }
    this.saveChanges = this.saveChanges.bind(this);
    this.cancelChanges = this.cancelChanges.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.setState({ inputFieldValue: e.target.value });
  }

  saveChanges() {
    this.props.saveChanges(this.state.inputFieldValue);
    this.props.updateMessage(this.props.message);
  }

  cancelChanges() {
    this.props.cancelChanges();
  }

  render() {

    return(
      <div className="EditForm">
        <div className="EditFormFrame">
          <textarea onKeyPress={this.onChange}>
            {this.props.message.text}
          </textarea>
          <button onClick={this.saveChanges}>Save</button>
          <button onClick={this.cancelChanges}>Cancel</button>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    message: state.editForm.currentMessage
  };
};

const mapDispatchToProps = {
  updateMessage, saveChanges, cancelChanges
};

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);
