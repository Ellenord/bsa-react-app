import { SHOW_FORM, SAVE_CHANGES, CANCEL_CHANGES } from './actionTypes';
import { getBeautyTime } from '../util';

const initialState = {
  isActive: false,
  currentMessage: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SHOW_FORM: {
      const { message } = action.payload;
      return { isActive: true, currentMessage: message };
    }
    case SAVE_CHANGES: {
      const { text } = action.payload;
      let newMessage = state.currentMessage;
      newMessage.text = text;
      newMessage.editedAt = getBeautyTime();
      return { isActive: false, currentMessage: newMessage };
    }
    case CANCEL_CHANGES: {
      return { isActive: false, currentMessage: state.currentMessage };
    }
    default:
      return state;
  }
};
