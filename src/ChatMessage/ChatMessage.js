import React from 'react';
import './ChatMessage.css';
import like from '../img/like.png';
import dislike from '../img/notlike.svg';

import { connect } from 'react-redux';
import { deleteMessage } from '../MessageList/actions';
import { showForm } from '../EditForm/actions';

class ChatMessage extends React.Component {

  constructor(props) {
    super(props);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
  }

  deleteMessage(e) {
    this.props.deleteMessage(this.props.message.id);
  }

  editMessage(e) {
    this.props.showForm(this.props.message);
  }

  render() {

    console.log('kek');

    const { id, userId, avatar, user, text, createdAt, editedAt } = this.props.message;

    return(
      <div className={'FlexWrapper' + (userId == '01234567-8901-2345-6789-012356789012' ? ' SelfMessage' : '')}>
        <div className='ChatMessage'>
          <div className="ChatMessageContent">
            <img src={avatar} />
            <p>
            <span>{user}</span><br/>
            {text}
            </p>
          </div>
          <div className="ChatMessageController">
            <i>{createdAt}</i>
            <label for={id}>
              <img className="ChatMessageLike" src={like} />
              <input type="checkbox" id={id} />
              <img className="ChatMessageDislike" src={dislike} />
            </label>
          </div>
          {userId == '01234567-8901-2345-6789-012356789012' &&
          <div className="SelfMessageController">
            <button onClick={this.deleteMessage}>Delete</button>
            <button onClick={this.editMessage}>Edit</button>
          </div>
          }
        </div>
        <div className="FreeSpace"></div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = {
  deleteMessage, showForm
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatMessage);
