import { INIT_MESSAGES, ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE } from './actionTypes.js';
import { beautyTime } from '../util';

const initialState = [];

export default function(state = initialState, action) {
  switch (action.type) {
    case INIT_MESSAGES: {
      let { messages } = action.payload;
      for (let i = 0; i < messages.length; ++i) {
        messages[i].id = String(i);
        messages[i].createdAt = beautyTime(messages[i].createdAt);
        messages[i].editedAt = beautyTime(messages[i].editedAt);
      }
      return messages;
    }
    case ADD_MESSAGE: {
      const { data } = action.payload;
      const id = state.length != 0 ? state[state.length - 1].id + 1 : 0;
      const message = { id, ...data };
      return [...state, message];
    }
    case UPDATE_MESSAGE: {
      const { message } = action.payload;
      const updated = state.map(i => {
        if (i.id == message.id) {
          return message;
        } else {
          return i;
        }
      });
      return updated;
    }
    case DELETE_MESSAGE: {
      const { id } = action.payload;
      return state.filter(message => message.id != id);
    }
    default:
      return state;
  }
};
