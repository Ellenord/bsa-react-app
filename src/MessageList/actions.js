import { INIT_MESSAGES, ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE } from './actionTypes';

export const initMessages = (messages) => ({
  type: INIT_MESSAGES,
  payload: {
    messages
  }
});

export const addMessage = (data) => ({
  type: ADD_MESSAGE,
  payload: {
    data
  }
});

export const updateMessage = (message) => ({
  type: UPDATE_MESSAGE,
  payload: {
    message
  }
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});
