import { combineReducers } from 'redux';
import messageList from './MessageList/reducer';
import editForm from './EditForm/reducer';

const rootReducer = combineReducers({
  messageList,
  editForm
});

export default rootReducer;
