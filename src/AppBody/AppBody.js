import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import './AppBody.css';

import { connect } from 'react-redux';

import Logo from '../Logo/Logo';
import Chat from '../Chat/Chat';
import Spinner from '../Spinner/Spinner';

import loadData from '../DataService/DataService';

import { initMessages } from '../MessageList/actions';

import EditForm from '../EditForm/EditForm';


class AppBody extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    loadData('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then(response => {
        return JSON.parse(response);
      })
      .then((response) => {
        this.props.initMessages(response);
        this.setState({ isLoading: false });
      });
  }

  render() {
    return(
      this.state.isLoading ? <Spinner /> :
      <div className="AppBody">
        <Logo height="48px"/>
        <Chat />
        {this.props.isActive && <EditForm />}
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    isActive: state.editForm.isActive
  };
};

const mapDispatchToProps = {
  initMessages
};

export default connect(mapStateToProps, mapDispatchToProps)(AppBody);
