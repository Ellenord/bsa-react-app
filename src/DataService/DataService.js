
async function loadData(url) {
  let response = await fetch(url);
  let text = await response.text();
  return text;
}

export default loadData;
